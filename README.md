# A Cold Walk
## An infinite console-based game written in Python

The winter has come, out of nowhere, and you are alone in a shack... For now.

Keep yourself warm, and make sure you don't run out of wood, food, and don't panic.

It's a scary world out there.

### Requirements:

* Python 2.7+ (But not Python 3.x)

**A Cold Walk should run under both Windows and Linux, however, it is only currently tested under Linux.**

### Running:

There are two methods.

1. From a Release

* [Download the Latest Release](https://github.com/shakna-israel/AColdWalk/releases), unless it includes "alpha" or "beta" in it's name.
* Unzip the folder.
* Open the folder.
* On Linux, you can simply open with ```./AColdWalk.py```, on Windows, you will need to open ```start.bat```.

2. From Git

*If you clone from the ```master``` branch, it is likely not to be stable.*

* Clone the repository
  * ```git clone https://github.com/shakna-israel/AColdWalk.git```
* Open the created folder.
* On Linux, you can simply open with ```./AColdWalk.py```, on Windows, you will need to open ```start.bat```.
