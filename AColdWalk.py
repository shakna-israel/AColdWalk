#!/usr/bin/env python
import os
import time
import random
import sys

global var_warmth
global var_wood
global var_health
global var_food
global var_anxiety
global username
global stranger
global friend
global event
global debug
global infloop
debug = 'false' # Debug is set in individual functions!
event = 0
load = 'false'
infloop = 'false'

def clear():
    os.system(['clear','cls'][os.name == 'nt'])

def init():
    global var_warmth
    var_warmth = 50
    global var_wood
    var_wood = 100
    global var_health
    var_health = 100
    global var_food
    var_food = 80
    global var_anxiety
    var_anxiety = 10
    global stranger
    stranger = 0
    global friend
    friend = 0

def fetch_status():
    checkValues()
    print "Your name is: " + username
    print "Body Warmth: " + str(var_warmth) + "%"
    print "Wood Stockpile: " + str(var_wood) + "%"
    print "Health: " + str(var_health) + "%"
    print "Food Stockpile: " + str(var_food) + "%"
    print "Anxiety Level: " + str(var_anxiety) + "%"
    print ""
    print ""

def reset_values():
    global var_warmth
    global var_wood
    global var_health
    global var_food
    global var_anxiety
    global stranger
    global friend
    friend = int(friend)
    stranger = int(stranger)
    var_anxiety = int(var_anxiety)
    var_food = int(var_food)
    var_health = int(var_health)
    var_wood = int(var_wood)
    var_warmth = int(var_warmth)

def checkValues():
    reset_values()
    global var_wood
    if var_wood >= 100:
        var_wood = 100
    if var_wood <= 0:
        var_wood = 0
    global var_warmth
    if var_warmth >= 100:
        var_warmth = 100
    if var_warmth <= 0:
        var_warmth = 0
    global var_health
    if var_health >= 100:
        var_health = 100
    if var_health <= 0:
        var_health = 0
    global var_food
    if var_food >= 100:
        var_food = 100
    if var_food <= 0:
        var_food = 0
    global var_anxiety
    if var_anxiety >= 100:
        var_anxiety = 100
    if var_anxiety <= 0:
        var_anxiety = 0
    global stranger
    if stranger >= 1:
        var_anxiety = var_anxiety + random.randint(0, 5)
    global friend
    if friend >= 1:
        var_anxiety = var_anxiety - friend

def stokeFire():
    clear()
    if friend < 1:
        print "The fire warms the room."
        global var_wood
        var_wood = var_wood - random.randint(0,10)
        global var_warmth
        var_warmth = var_warmth + random.randint(0,10)
    if friend == 1:
        print "The cold brings friends together."
        global var_wood
        var_wood = var_wood - random.randint(0,5)
        global var_warmth
        var_warmth + random.randint(5,10)
    if friend > 1:
        print "Nothing is warmer than friendship."
        global var_wood
        global friend
        friend_multiplier = friend * random.randint(0,10)
        friend_divider = friend / random.randint(0,10)
        var_wood = var_wood - random.randint(0,friend_divider)
        global var_warmth
        var_warmth + random.randint(friend_multiplier,friend_multiplier)

def gatherWood():
    clear()
    if friend < 1:
        print "It's freezing out here!"
        print "... Lucky there's so much bracken."
        global var_wood
        var_wood = var_wood + random.randint(0,10)
        global var_warmth
        var_warmth = var_warmth - random.randint(0,10)
    if friend == 1:
        print "It might be cold, but at least you have company."
        global var_wood
        var_wood = var_wood + random.randint(5,10)
        global var_warmth
        var_warmth = var_warmth - random.randint(0,10)
    if friend > 1:
        print "Many hands make light work."
        global var_wood
        global friend
        var_wood = var_wood + random.randint(0,friend)
        global var_warmth
        var_warmth = var_warmth - random.randint(0,10)

def save_game():
    try:
        file = open("save.var", "w")
        global var_warmth
        global var_wood
        global var_health
        global var_food
        global var_anxiety
        global username
        global stranger
        global friend
        global event
        file.write(str(var_warmth))
        file.write("\n")
        file.write(str(var_wood))
        file.write("\n")
        file.write(str(var_health))
        file.write("\n")
        file.write(str(var_food))
        file.write("\n")
        file.write(str(var_anxiety))
        file.write("\n")
        file.write(str(username))
        file.write("\n")
        file.write(str(stranger))
        file.write("\n")    
        file.write(str(friend))
        file.write("\n")
        file.write(str(event))
        file.write("\n")
        file.close()
    except:
         print "Can't open file for writing! Is it in use?"

def load_game():
    try:
        global var_warmth
        global var_wood
        global var_health
        global var_food
        global var_anxiety
        global username
        global stranger
        global friend
        global event
        global load
        if debug == 'true':
            print "Load start"
        load = 'true'
        if debug == 'true':
            print "Load set to..." + load
        var_warmth = 0
        var_wood = 0
        var_health = 0
        var_food = 0
        var_anxiety = 0
        username = 0
        stranger = 0
        friend = 0
        event = 0
        if debug == 'true':
            print "Values instantiated."
        file = open("save.var", "r")
        i = 0
        for line in file.read().split('\n'):
	    if i == 0:
                var_warmth = line
                if debug == 'true':
                    print "var_warmth... " + var_warmth
            elif i == 1:
                var_wood = line
            elif i == 2:
                var_health = line
            elif i == 3:
                var_food = line
            elif i == 4:
                var_anxiety = line
            elif i == 5:
                username = line
            elif i == 6:
                stranger = line
            elif i == 7:
                friend = line
            elif i == 8:
                event = line
                if debug == 'true':
                    print "event set to... " + event
            i = i + 1
        file.close()
        if debug == 'true':
            print "File closed."
        if debug == 'true':
	    print "Attempting to start game_story()"
        game_story()
    except:
        print "Savefile not found!"
        print "Starting new game."
        time.sleep(2)
        new_game()

def actionChoice():
    checkValues()
    clear()
    fetch_status()
    if friend < 1:
        print "Enter A to stoke the fire."
        print "Enter B to gather more wood."
    elif friend == 1:
        print "Enter A to snuggle by the fire."
        print "Enter B to gather wood together."
    elif friend >= 1:
        print "Enter A to group by the fire."
        print "Enter B to gather wood together."
    print "Enter S to save."
    print "Enter Q to quit. (Does not save)"
    choice_active = 1
    while (choice_active == 1):
        choice = raw_input("... ")
        if choice == 'A':
            stokeFire()
            choice_active = 0
        elif choice == 'a':
            stokeFire()
            choice_active = 0
        elif choice == 'B':
            gatherWood()
            choice_active = 0
        elif choice == 'b':
            gatherWood()
            choice_active = 0
        elif choice == 'S':
            save_game()
            print "Saved"
        elif choice == 's':
            save_game()
            print "Saved"
        elif choice == 'Q':
            sys.exit("Quit without error.")
        elif choice == 'q':
            sys.exit("Quit without error.")
    print raw_input("Press enter to continue.")
    clear()

def play_loop():
    clear()
    fetch_status()
    global infloop
    if event == 'one':
        loop = random.randint(0,20)
    if event == 'two':
        loop = random.randint(20,50)
    if event == 'three':
        loop = random.randint(50,100)
    else:
        loop = 2
        infloop = 'true'
    while loop > 1:
        if infloop == 'false':
            loop = loop - 1
        actionChoice()

def game_story():
    global load
    global event
    debug = 'true'
    if load == 'true':
        if debug == 'true':
            print "Load is true."
    	if event == 'one':
            if debug == 'true':
            	print "Event is one."
            play_loop()
    	    event2()
    	    load = 0
    	elif event == 'two':
    	     if debug == 'true':
                 print "Event is two"
             play_loop()
    	     event3()
    	     load = 0
        else:
            if debug == 'true':
                print "No event found in save. Preserving values are restarting function..."
            global load
            load = 'false'
            game_story()
    else:
        if debug == 'true':
            print "Haven't found a specific event, continuing..."
	play_loop()
    	event1()
    	play_loop()
    	event2()
    	play_loop()
    	event3()

def new_game():
    init()
    print "... What is your name?"
    global username
    username = raw_input("... ")
    clear()
    print "It all happened on a cold day in July."
    print ""
    print "... I mean a REALLY cold day."
    print "The rain would move to hail and back without warning."
    print ""
    print ""
    print raw_input("Press enter to continue.")
    clear()
    print "The rain just kept coming down, and the day kept getting colder."
    print ""
    print ""
    print "... The cabin I, " + username + ", was staying in... I guess you could say it wasn't going to stay in one piece much longer."
    print raw_input("Press enter to continue.")
    game_story()

def event1():
     global event
     event = 1
     clear()
     print "The wind has picked up, the cabin is really not liking it."
     print ""
     print "Is it hailing again?"
     print ""
     print ""
     print raw_input("Press enter to continue.")
     clear()
     print "Oh hell..."
     print ""
     print ""
     print raw_input("Press enter to continue.")
     clear()
     print "Oh hell..."
     print ""
     print "There's someone at the door..."
     print ""
     print ""
     print raw_input("Press enter to continue.")
     clear()
     print "I..."
     print ""
     print ""
     print raw_input("Press enter to continue.")
     clear()
     print "I... I guess I can't leave them out there."
     print ""
     print "Not in this weather."
     print ""
     print ""
     print raw_input("Press enter to continue.")
     clear()
     print "I opened the door, and the stranger stumbled in."
     global stranger
     stranger = 1
     print ""
     print "They collapsed by the fire and passed out..."
     print ""
     print ""
     print raw_input("Press enter to continue.")
     play_loop()
     event2()

def event2():
     global event
     event = 2
     clear()
     print "The stranger by the fire is waking up..."
     print ""
     print ""
     print raw_input("Press enter to continue.")
     clear()
     global var_anxiety
     var_anxiety = var_anxiety + var_anxiety
     fetch_status()
     print ""
     print "The stranger turns to me and smiles weakly... "
     print ""
     print raw_input("Press enter to continue.")
     clear()
     var_anxiety = var_anxiety + var_anxiety
     fetch_status()
     print ""
     print "The stranger turns to me and smiles weakly... and aplogises."
     print ""
     print ""
     print raw_input("Press enter to continue.")
     var_anxiety = random.randint(0,20)
     fetch_status()
     print ""
     print "They say their name is Amanda Lovine. They got caught in the storm."
     global username
     print "I say my name is " + username
     print "Amanda says she can help with the wood and the fire."
     print ""
     print ""
     print raw_input("Press enter to continue.")
     play_loop()
     event3()

def event3():
    clear()
    global event
    event = 3
    print "After all that, Amanda and I?"
    print ""
    print "We're friends."
    print ""
    print ""
    print raw_input("Press enter to continue.")
    global stranger
    stranger = stranger - 1
    global friend
    friend = friend + 1
    play_loop()

# Global function for game intro
clear()
print "A Cold Walk"
time.sleep(1)
print ""
print ""
print "jm | Design"
time.sleep(1)
clear()
print "Loading"
time.sleep(1)
clear()
print "Loading."
time.sleep(1)
clear()
print "Loading.."
time.sleep(1)
clear()
print "Loading..."
time.sleep(1)
clear()
print "Enter 1 to Load Game."
print "Enter 2 for a New Game."
choice_active = "active"
while (choice_active == "active"):
    choice = raw_input("... ")
    if choice == '1':
        load_game()
    elif choice == '2':
        new_game()
