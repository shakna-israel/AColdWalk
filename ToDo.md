v 0.0.2

* [Nosetest](https://github.com/shakna-israel/AColdWalk/issues/7)
* [Story Needs Extending](https://github.com/shakna-israel/AColdWalk/issues/4)
* [Food Not Implemented](https://github.com/shakna-israel/AColdWalk/issues/3)

V 0.0.3

* [No Random Events](https://github.com/shakna-israel/AColdWalk/issues/10)
* [Python 3.x Support](https://github.com/shakna-israel/AColdWalk/issues/6)
* [Restructuring Necessary](https://github.com/shakna-israel/AColdWalk/issues/2)
